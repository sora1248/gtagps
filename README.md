# GTA-GPS

GPS-Logger for Grand Theft Auto.

This program reads the player's current location ten times per second and prints the location when the player has moved at least 1 meter.

Currently only GTA III v1.1 and GTA Vice City are supported and it still might fail.

# Anti-Virus software

The logger makes use of `ReadProcessMemory` to extract the current location of the player. This might result in false alarms by Anti-Virus software.

# Installation

1. Install the mingw-toolchain:
```
sudo apt install g++-mingw-w64
```

2. Compile the logger:
```
make
```

This should also compile under Windows with MinGW and probably also with Visual Studio, but I haven't tested these.

# Usage

1. Run `logger.exe` and it will start GTA3 and print the player's location to stdout. Redirect it to a file to create a log.

## Create a Heatmap

I managed to create a heatmap using `heatmap.py` from http://www.sethoscope.net/heatmap/

1. Use `log2csv.py` to convert the coordinates: Pipe the log or call it with the names of the logs:
```
log2csv.py mygtalogs/*.log > gta.csv
```

2. Call `heatmap.py` to generate a heatmap:
```
path_to_heatmap/heatmap.py gta.csv -P equirectangular -W 1024 -o heatmap.png
```

This will generate a heatmap which is 1024px wide.

If you have the radar map of GTA III you can use it as background:
```
path_to_heatmap/heatmap.py dump.csv -P equirectangular -e="-2,-2,2,2" -I GTAIII_Radarmap.png  -o heatmap.png
```

# Issues/ToDo

- Only GTA III and Vice City are supported so far.
- Works only with a specific version of gta3.exe/gta-vc.exe due to the fixed pointer addresses. Possible solutions:
  - Detect which version of the game is installed and change the pointer accordingly.
  - Automatically search for the pointer.
- Create a log automatically.
- I've only tested this with Windows XP - This might not work with newer Windows versions. On the other hand: I had to use XP as I couldn't get GTA III working on Win 7.
