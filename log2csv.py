#!/usr/bin/python3

import fileinput

for line in fileinput.input():
	x, y, z = (float(i) for i in line.split(','))
	print("{},{}".format(y / 1000, x / 1000))