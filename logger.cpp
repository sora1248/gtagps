#include <iostream>
#include <windows.h>
#include <cmath>
#include <experimental/filesystem>

namespace fs = std::experimental::filesystem;

struct Position {
	float x;
	float y;
	float z;
};

static_assert(sizeof(Position) == 12);

static double distance(const Position &pos1, const Position &pos2) {
	const auto sqr = [](const double a) { return a * a; };

	return std::sqrt(sqr(pos1.x - pos2.x) + sqr(pos1.y - pos2.y) + sqr(pos1.z - pos2.z));
};

class Game
{
public:
	Game(const fs::path &filename, const void * const address_pCPed, const size_t offset) :
		address_pCPed(address_pCPed), offset(offset)
	{
		STARTUPINFOW startupinfo = { sizeof(STARTUPINFOW) };
		if (!CreateProcessW(filename.c_str(), nullptr, nullptr, nullptr, FALSE, 0, nullptr, nullptr, &startupinfo, &process_information)) {
			throw std::runtime_error("Process creation failed.");
		}
	}

	~Game()
	{
		CloseHandle(process_information.hProcess);
		CloseHandle(process_information.hThread);
	}

	static std::unique_ptr<Game> Detect() {
		// TODO Detect different versions of the games!
		if (fs::exists(L"gta3.exe")) {
			return std::make_unique<Game>(L"gta3.exe", reinterpret_cast<void *>(0x9404a8), 0x34);
		}
		if (fs::exists(L"gta-vc.exe")) {
			return std::make_unique<Game>(L"gta-vc.exe", reinterpret_cast<void *>(0x949d30), 0x34);
		}
		return nullptr;
	}

	bool is_running(const int timeout = 100) const {
		return WaitForSingleObject(process_information.hProcess, timeout) == WAIT_TIMEOUT;
	}

	bool get_position(Position &pos) const
	{
		void * CPed_base;
		if ((!ReadProcessMemory(process_information.hProcess, address_pCPed, &CPed_base, 4, nullptr)) || (!CPed_base)) {
			return false;
		}

		void * CPed_position = reinterpret_cast<char *>(CPed_base) + offset;
		return ReadProcessMemory(process_information.hProcess, CPed_position, &pos, 12, nullptr) != 0;
	}

private:
	const void * address_pCPed;
	size_t offset;
	PROCESS_INFORMATION process_information = {};
};

static void update_position(const Position &pos) {
	static Position last_pos = {};
	if (distance(last_pos, pos) >= 1.0) {
		last_pos = pos;
		std::cout <<  pos.x << "," << pos.y << "," << pos.z << std::endl;
	}
}

int main(int argc, char ** argv)
{
	std::unique_ptr<Game> game = Game::Detect();

	if (!game) {
		std::cerr << "Could not detect game!\n";
		return 1;
	}

	while (game->is_running()) {
		Position pos;
		if (game->get_position(pos)) {
			update_position(pos);
		}
	}

	return 0;
}
